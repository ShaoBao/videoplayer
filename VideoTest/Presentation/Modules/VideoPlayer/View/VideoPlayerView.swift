//
//  VideoPlayerView.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit


protocol VideoPlayerView: class {
    func layout(newSize: CGSize)
}
