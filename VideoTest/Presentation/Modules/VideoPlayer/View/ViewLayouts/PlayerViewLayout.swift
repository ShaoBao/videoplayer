//
//  PlayerViewLayout.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


struct PlayerViewLayout {
    
    func initialPlayerPresets(_ view: UIView) {
        guard let superview = view.superview else { return }
        view.topAnchor.constraint(equalTo: superview.centerYAnchor, constant: 0)
            .isActive = true
        view.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 0)
            .isActive = true
        view.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: 0)
            .isActive = true
        view.heightAnchor.constraint(equalTo: superview.heightAnchor, multiplier: 0.5)
            .isActive = true
    }
}
