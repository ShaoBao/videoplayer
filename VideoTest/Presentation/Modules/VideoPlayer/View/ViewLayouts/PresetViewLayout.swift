//
//  PresetViewLayout.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit


struct PresetViewLayout {
    
    func initial(_ view: UIButton) {
        guard  let superview = view.superview else { return }
        view.topAnchor.constraint(equalTo: superview.topAnchor, constant: 16)
        .isActive = true
        view.leftAnchor.constraint(equalTo: superview.centerXAnchor, constant: -16)
        .isActive = true
        view.widthAnchor.constraint(equalToConstant: 50)
        .isActive = true
        view.heightAnchor.constraint(equalToConstant: 50)
        .isActive = true
    }
    
    func initialSlider(_ view: UISlider) {
        guard let superview = view.superview else { return }
        view.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0)
        .isActive = true
        view.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 16)
        .isActive = true
        view.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: -16)
        .isActive = true
        view.heightAnchor.constraint(equalToConstant: 30)
        .isActive = true
    }
}
