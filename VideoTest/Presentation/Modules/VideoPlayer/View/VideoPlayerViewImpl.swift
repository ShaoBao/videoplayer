//
//  VideoPlayerViewImpl.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class VideoPlayerViewImpl: UIView {
    
    var viewModel: VideoPlayerViewModel? {
        didSet {
            updateSubViews()
        }
    }
    
    var videoService     : VideoPlayerService?
    var playerPresetView : PlayerPresetViewImpl?
    var playerViewlayout = PlayerViewLayout()
    
    let sampleURL = URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")
    
    override func didMoveToSuperview() {
        setupViews()
        videoService?.setupMainView(mainView: self)
        videoService?.prepareVideo(url: sampleURL!)
        videoService?.setupPresetView(presetView: playerPresetView!)
        bringSubviewToFront(playerPresetView!)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    
    init(videoService: VideoPlayerService?, frame: CGRect) {
        super.init(frame: frame)
        self.videoService = videoService
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateSubViews() {
        guard let urlPath = viewModel?.url else   { return }
        guard let url = URL(string: urlPath) else { return }
    }
    
    private func setupViews() {
        setupPlayerPresetView()
    }
    
    private func setupPlayerPresetView() {
        playerPresetView = PlayerPresetViewImpl()
        playerPresetView?.translatesAutoresizingMaskIntoConstraints = false
        addSubview(playerPresetView!)
        playerViewlayout.initialPlayerPresets(playerPresetView!)
    }
    
}

extension VideoPlayerViewImpl: VideoPlayerView {
    func layout(newSize: CGSize) {
        self.frame.size = newSize
        self.videoService?.changeLayerAfterRotate(value: newSize)
    }
}
