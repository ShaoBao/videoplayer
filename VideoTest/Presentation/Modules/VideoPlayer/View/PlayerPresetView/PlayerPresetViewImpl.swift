//
//  PlayerPresetView.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit

class PlayerPresetViewImpl: UIView {
    
    var startStopBtn        : UIButton?
    var slider              : UISlider?
    var isPlaying           = false
    var roundCorners        : CGFloat?
    var buttonCorneredRadius: CGFloat  = 10.0
    var transparentAlpha    : CGFloat  = 0.2
    var fullAlpha           : CGFloat  = 1.0
    
    let presetLayout = PresetViewLayout()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners = (self.startStopBtn?.frame.size.width)! / 2
        self.startStopBtn?.layer.cornerRadius = roundCorners ?? 0
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        setupStartStopBtn()
        setupSlider()
    }
    
    private func setupSlider() {
        slider = UISlider()
        slider?.translatesAutoresizingMaskIntoConstraints = false
        slider?.addTarget(self, action: #selector(sliderValueChangedByUser), for: .valueChanged)
        self.addSubview(slider!)
        presetLayout.initialSlider(slider!)
    }
    
    private func setupStartStopBtn() {
        startStopBtn = UIButton()
        startStopBtn?.setTitle("Play", for: .normal)
        startStopBtn?.backgroundColor = .darkGray
        startStopBtn?.translatesAutoresizingMaskIntoConstraints = false
        startStopBtn?.addTarget(self, action: #selector(playPause), for: .touchUpInside)
        self.addSubview(startStopBtn!)
        presetLayout.initial(startStopBtn!)
    }
    
    @objc func sliderValueChangedByUser() {
        let sliderValue = ["value": slider?.value]
        NotificationCenter.default.post(name: NotificationConstant.Name.notifySliderChanged, object: nil, userInfo:  sliderValue as [AnyHashable : Any])
    }
    
    @objc func playPause() {
        NotificationCenter.default.post(name:NotificationConstant.Name.notifiyPlayStop, object: nil)
        transfromBtn()
    }
    
    private func transfromBtn() {
        if isPlaying {
            UIView.animate(withDuration: 0.4) {
                self.startStopBtn?.layer.cornerRadius = self.roundCorners ?? 0
                self.startStopBtn?.setTitle("Play", for: .normal)
                self.startStopBtn?.alpha = self.fullAlpha
            }
            isPlaying = false
        } else {
            UIView.animate(withDuration: 0.4) {
                self.startStopBtn?.layer.cornerRadius = self.buttonCorneredRadius
                self.startStopBtn?.setTitle("Pause", for: .normal)
                self.startStopBtn?.alpha = self.transparentAlpha
            }
            isPlaying = true
        }
    }
}
extension PlayerPresetViewImpl: PlayerPresetView {
    func sliderValueChanged(value: Double) {
        self.slider?.value = Float(value)
    }
}
