//
//  PlayerPresetView.swift
//  VideoTest
//
//  Created by Gleb on 21/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


protocol PlayerPresetView: class {
    func sliderValueChangedByUser()
    func playPause()
    func sliderValueChanged(value: Double)
    var isPlaying: Bool { get set}
}
