//
//  VideoPlayerViewControllerImpl.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit



class VideoPlayerViewControllerImpl: UIViewController {
    
    var interactor             : VideoPlayerInteractor!
    private var videoPlayerView: VideoPlayerView!
    var videoService           : VideoPlayerService?
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    init(videoService: VideoPlayerService) {
        super.init(nibName: .none, bundle: .none)
        self.videoService = videoService
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { [weak self] (context) in
            self?.videoPlayerView.layout(newSize: size)
            }
        )}
    
    private func setupView() {
        let videoPlayerView = VideoPlayerViewImpl(videoService: self.videoService ?? nil, frame: .zero)
        videoPlayerView.frame = view.bounds
        videoPlayerView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.videoPlayerView = videoPlayerView
        view.addSubview(videoPlayerView)
    }
}
