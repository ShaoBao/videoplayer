//
//  VideoPlayerInteractorImpl.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class VideoPlayerInteractorImpl {
    
    private let videoService   : VideoPlayerService
    private let presenter      : VideoPlayerPresenter
    weak var viewController    : VideoPlayerViewControllerImpl?
    
    init(videoService: VideoPlayerService, presenter: VideoPlayerPresenter) {
        self.videoService = videoService
        self.presenter = presenter
    }
}

extension VideoPlayerInteractorImpl: VideoPlayerInteractor {
    
}
