//
//  VideoPlayerAssembly.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class VideoPlayerAssembly {
    static func configureModule() -> VideoPlayerViewControllerImpl {
        
        let videoService = VideoPlayerServiceImpl()
        let viewController = VideoPlayerViewControllerImpl(videoService: videoService)
        let router = VideoPlayerRouterImpl(viewController: viewController)
        let presenter = VideoPlayerPresenterImpl(controller: viewController, router: router)
        let interactor = VideoPlayerInteractorImpl(videoService: videoService, presenter: presenter)
        
        viewController.interactor = interactor
        interactor.viewController = viewController
        presenter.viewController = viewController
        router.viewController = viewController
        
        return viewController
    }
}
