//
//  VideoPlayerRouterImpl.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation



class VideoPlayerRouterImpl {
    
    
    weak var viewController: VideoPlayerViewControllerImpl?
    
    init(viewController: VideoPlayerViewControllerImpl) {
        self.viewController = viewController
    }
    
}

extension VideoPlayerRouterImpl: VideoPlayerRouter {
    
}
