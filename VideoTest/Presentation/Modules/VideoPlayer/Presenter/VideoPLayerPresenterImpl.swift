//
//  VideoPLayerPresenterImpl.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation

class VideoPlayerPresenterImpl {
    
    
    weak var viewController: VideoPlayerViewControllerImpl?
    private let router     : VideoPlayerRouter
    
    init(controller: VideoPlayerViewControllerImpl, router: VideoPlayerRouter) {
        self.viewController = controller
        self.router = router
    }
}

extension VideoPlayerPresenterImpl: VideoPlayerPresenter {
    
}

