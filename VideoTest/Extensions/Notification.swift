//
//  Constants.swift
//  VideoTest
//
//  Created by Gleb on 23/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


public extension Notification.Name {
    static let notifiyPlayStop = Notification.Name("notifiyPlayStop")
    static let notifySliderChanged = Notification.Name("notifySliderChanged")
}
