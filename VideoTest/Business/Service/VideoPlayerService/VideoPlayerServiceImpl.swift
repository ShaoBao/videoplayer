//
//  VideoPlayerService.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit
import UIKit

class VideoPlayerServiceImpl {
    
    var player                    : AVPlayer?
    var playerLayer               : AVPlayerLayer?
    var isPlaying                 = false
    weak var currentViewForPlayer : VideoPlayerViewImpl?
    weak var presetView           : PlayerPresetView?
    
    init() {
        setupPlayer()
        setupNotifiers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NotificationConstant.Name.notifiyPlayStop, object: nil)
        NotificationCenter.default.removeObserver(self, name: NotificationConstant.Name.notifySliderChanged, object: nil)
    }
    
    private func setupNotifiers() {
        NotificationCenter.default.addObserver(self, selector: #selector(playPause), name: NotificationConstant.Name.notifiyPlayStop, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notifySliderChanged), name: NotificationConstant.Name.notifySliderChanged, object: nil)
    }

    private func setupPlayer() {
        player = AVPlayer()
    }
}

extension VideoPlayerServiceImpl: VideoPlayerService {
    func changeLayerAfterRotate(value: CGSize) {
        self.playerLayer?.frame.size = value
    }
    
    func setupPresetView(presetView: PlayerPresetView) {
        self.presetView = presetView
    }
    
    func setupMainView(mainView: VideoPlayerView) {
        self.currentViewForPlayer = mainView as? VideoPlayerViewImpl
    }

    func prepareVideo(url: URL) {
        self.player = AVPlayer(url: url)
        self.playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = self.currentViewForPlayer!.bounds
        let interval = CMTime(value: 1, timescale: 2)
        player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: {[weak self] (progressTime) in
            let seconds = CMTimeGetSeconds(progressTime)
            if let duration = self?.player?.currentItem?.duration {
                let durationInSeconds = CMTimeGetSeconds(duration)
                let neededTime = seconds / durationInSeconds
                self?.presetView?.sliderValueChanged(value: neededTime)
            }
        })
        playerLayer?.videoGravity = .resizeAspectFill
        currentViewForPlayer?.layer.addSublayer(playerLayer!)
    }
    
    @objc func playPause() {
        if player?.rate != 0 && player?.error == nil {
            player?.pause()
        } else {
            player?.play()
        }
    }
    
    @objc func notifySliderChanged(notification: Notification) {
        player?.pause()
        guard let sliderValue = notification.userInfo?["value"] as? Float else {
            return
        }
        if let duration = player?.currentItem?.duration {
            let totalSeconds = CMTimeGetSeconds(duration)
            let value = Float64(sliderValue) * totalSeconds
            let seekTime = CMTime(value: Int64(value), timescale: 1)
            player?.seek(to: seekTime, completionHandler: { (completed) in
                //Temporary not in use (!!!)
            })
        }
        if presetView!.isPlaying {
            player?.play()
        }
    }
}
