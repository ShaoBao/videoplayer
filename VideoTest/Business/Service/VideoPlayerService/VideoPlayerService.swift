//
//  VideoPlayerService.swift
//  VideoTest
//
//  Created by Gleb on 19/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit



protocol VideoPlayerService: class {
    func playPause()
    func prepareVideo(url : URL)
    func setupMainView(mainView: VideoPlayerView)
    func setupPresetView(presetView: PlayerPresetView)
    func changeLayerAfterRotate(value: CGSize)
}
