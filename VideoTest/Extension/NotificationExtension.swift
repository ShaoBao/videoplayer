//
//  NotificationExtension.swift
//  VideoTest
//
//  Created by Глеб Мороз on 7/3/20.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


struct NotificationConstant {
    
    struct Name {
        static let notifiyPlayStop: NSNotification.Name = NSNotification.Name("notifiyPlayStop")
        static let notifySliderChanged: NSNotification.Name = NSNotification.Name("notifySliderChanged")
    }
    
}
